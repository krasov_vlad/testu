package com.krasov.testapp.data.source.local

import com.krasov.testapp.domain.model.Comment
import com.krasov.testapp.domain.model.Post
import com.krasov.testapp.domain.model.User
import io.reactivex.Single
import java.lang.Exception

class MemoryStorage : ILocalStorage {

    private val _savedPosts = mutableSetOf<Post>()
    private val _savedUsers = mutableSetOf<User>()
    private val _savedComments = mutableSetOf<Comment>()

    override fun getPosts(): List<Post> {
        return _savedPosts.toList()
    }

    override fun getPost(postId: Int): Post? {
        return _savedPosts.find { it.id == postId }
    }

    override fun getUser(userId: Int): User? {
        return _savedUsers.find { it.id == userId }
    }

    override fun getComments(postId: Int): List<Comment> {
        return _savedComments.filter { it.postId == postId }
    }

    override fun savePosts(vararg post: Post) {
        _savedPosts.addAll(post)
    }

    override fun saveUser(user: User) {
        _savedUsers.add(user)
    }

    override fun saveComments(list: List<Comment>) {
        _savedComments.addAll(list)
    }

    override fun clear() {
        _savedPosts.clear()
        _savedUsers.clear()
        _savedComments.clear()
    }
}