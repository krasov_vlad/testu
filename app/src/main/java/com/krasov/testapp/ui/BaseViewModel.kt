package com.krasov.testapp.ui

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BaseViewModel : ViewModel() {
    private var compositeDisposable = CompositeDisposable()


    fun subscribe(vararg disposable: Disposable) {
        compositeDisposable.addAll(compositeDisposable)
    }


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}