package com.krasov.testapp.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.krasov.testapp.R
import com.krasov.testapp.domain.model.Post
import kotlinx.android.synthetic.main.list_fragment.*
import org.koin.androidx.scope.currentScope


class ListFragment : Fragment() {

    private val viewModel: ListViewModel by currentScope.inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.list_fragment, container, false)
    }

    private val postsAdapter by lazy {
        PostsAdapter()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.loadPosts(false)

        viewModel.toast.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let {
                Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
            }
        })

        viewModel.posts.observe(viewLifecycleOwner, Observer {
            postsAdapter.setPosts(it)
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        postsAdapter.setOnItemSelectListener {
            findNavController().navigate(ListFragmentDirections.toDetailFragment(it.id))
        }

        postList.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = postsAdapter
        }

        refresh.setOnClickListener {
            viewModel.loadPosts(true)
        }
    }


}