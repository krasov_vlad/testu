package com.krasov.testapp.utils.livedata

import androidx.lifecycle.LiveData
import com.krasov.testapp.utils.applyBackgroundTaskSchedulers
import io.reactivex.Single
import io.reactivex.disposables.Disposable

class RxSingleLiveData<T>(
    private val call: Single<T>,
    private val onError: (t: Throwable) -> Unit = {}
) : LiveData<T>() {
    private var disposable: Disposable? = null
    override fun onActive() {
        super.onActive()
        disposable?.dispose()
        disposable = call
            .applyBackgroundTaskSchedulers()
            .subscribe({
                value = it
            }, {
                onError.invoke(it)
            })
    }

    override fun onInactive() {
        super.onInactive()
        disposable?.dispose()
    }
}