package com.krasov.testapp.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.krasov.testapp.domain.MainInteractor
import com.krasov.testapp.domain.model.Comment
import com.krasov.testapp.domain.model.Post
import com.krasov.testapp.domain.model.User
import com.krasov.testapp.ui.BaseViewModel
import com.krasov.testapp.utils.applyBackgroundTaskSchedulers
import com.krasov.testapp.utils.livedata.SingleEvent
import io.reactivex.Single
import io.reactivex.functions.BiFunction

class DetailViewModel(private val mainInteractor: MainInteractor, private val postId: Int) :
    BaseViewModel() {

    private val _comments = MutableLiveData<List<Comment>>()
    val comments: LiveData<List<Comment>> = _comments

    private val _post = MutableLiveData<Post>()
    val post: LiveData<Post> = _post

    private val _user = MutableLiveData<User>()
    val user: LiveData<User> = _user

    private val _toast = MutableLiveData<SingleEvent<String>>()
    val toast: LiveData<SingleEvent<String>> = _toast

    val showLoading = MutableLiveData<Boolean>()

    fun loadData(force: Boolean) {
        subscribe(
            Single.zip(
                mainInteractor.getPost(postId, force || post.value == null),
                mainInteractor.getComments(postId, force || comments.value == null),
                BiFunction { t1: Post, t2: List<Comment> -> Pair(t1, t2) }
            )
                .doOnSuccess {
                    _post.postValue(it.first)
                    _comments.postValue(it.second)
                }
                .flatMap { mainInteractor.getUser(it.first.userId, force || user.value == null) }
                .doOnSuccess { _user.postValue(it) }
                .applyBackgroundTaskSchedulers()
                .doOnSubscribe { showLoading.value = true }
                .subscribe({
                    showLoading.value = false
                }, {
                    //todo handle error properly
                    showLoading.value = false
                    _toast.value = SingleEvent(it.message ?: "unknown error")
                })
        )
    }
}