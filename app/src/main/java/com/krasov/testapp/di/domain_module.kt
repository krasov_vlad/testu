package com.krasov.testapp.di

import com.krasov.testapp.data.repository.MainRepository
import com.krasov.testapp.domain.MainInteractor
import com.krasov.testapp.domain.repository.IRepository
import org.koin.dsl.module


val domainModule = module {
    single<IRepository> { MainRepository(get(), get()) }
    single { MainInteractor(get()) }
}