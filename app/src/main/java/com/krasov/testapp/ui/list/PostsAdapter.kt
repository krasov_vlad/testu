package com.krasov.testapp.ui.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.krasov.testapp.domain.model.Post

typealias OnItemSelected = (Post) -> Unit

class PostsAdapter() : RecyclerView.Adapter<PostsAdapter.PostViewHolder>() {

    private var posts = listOf<Post>()

    private var onItemSelectedListener: OnItemSelected? = null

    fun setPosts(list: List<Post>) {
        //todo use diffutils here
        posts = list
        notifyDataSetChanged()
    }

    fun setOnItemSelectListener(l: OnItemSelected) {
        onItemSelectedListener = l
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        return PostViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(android.R.layout.simple_list_item_2, parent, false)
        )
    }

    override fun getItemCount(): Int = posts.size

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        val post = posts[position]
        holder.bind(post)
        holder.itemView.setOnClickListener {
            onItemSelectedListener?.invoke(post)
        }
    }

    class PostViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(post: Post) {
            view.findViewById<TextView>(android.R.id.text1)?.text = post.title
            view.findViewById<TextView>(android.R.id.text2)?.text = post.body
        }
    }
}