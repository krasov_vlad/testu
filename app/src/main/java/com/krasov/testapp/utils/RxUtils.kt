package com.krasov.testapp.utils

import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.Schedulers.io

fun <T> Observable<T>.applyBackgroundTaskSchedulers(): Observable<T> = subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())

fun <T> Single<T>.applyBackgroundTaskSchedulers(): Single<T> = subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())

fun <T> Maybe<T>.applyBackgroundTaskSchedulers(): Maybe<T> = subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())

fun <T> Flowable<T>.applyBackgroundTaskSchedulers(): Flowable<T> = subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())

fun  Completable.applyBackgroundTaskSchedulers(): Completable = subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())