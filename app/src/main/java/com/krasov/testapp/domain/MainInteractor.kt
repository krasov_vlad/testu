package com.krasov.testapp.domain

import com.krasov.testapp.domain.model.Comment
import com.krasov.testapp.domain.model.Post
import com.krasov.testapp.domain.model.User
import com.krasov.testapp.domain.repository.IRepository
import io.reactivex.Single

//TODO we can separate MainRepositroy to post\user\comments repos
//todo then separate maininteractor to featuche (list\detail) usecases
//todo for detailusecase we can map post\user\comments to some combinemodel
class MainInteractor(private val repository: IRepository) {

    fun getPost(postId: Int, forceLoad: Boolean = false): Single<Post> {
        return repository.getPost(postId, forceLoad)
    }

    fun getUser(userId: Int, forceLoad: Boolean = false): Single<User> {
        return repository.getUser(userId, forceLoad)
    }

    fun getPosts(forceLoad: Boolean = false): Single<List<Post>> {
        return repository.getPosts(forceLoad)
    }

    fun getComments(postId: Int, forceLoad: Boolean = false): Single<List<Comment>> {
        return repository.getComments(postId, forceLoad)
    }
}