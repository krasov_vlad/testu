package com.krasov.testapp.domain.repository

import com.krasov.testapp.domain.model.Comment
import com.krasov.testapp.domain.model.Post
import com.krasov.testapp.domain.model.User
import io.reactivex.Single

interface IRepository {
    fun getUser(userId: Int, forceNetwork: Boolean): Single<User>
    fun getPost(postId: Int, forceNetwork: Boolean): Single<Post>
    fun getPosts(forceNetwork: Boolean): Single<List<Post>>
    fun getComments(postId: Int,forceNetwork: Boolean): Single<List<Comment>>
}