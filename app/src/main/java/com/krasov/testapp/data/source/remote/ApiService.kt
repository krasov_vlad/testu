package com.krasov.testapp.data.source.remote

import com.krasov.testapp.domain.model.Comment
import com.krasov.testapp.domain.model.Post
import com.krasov.testapp.domain.model.User
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {
    @GET("/posts")
    fun getPosts(): Single<List<Post>>

    @GET("/posts/{postId}")
    fun getPost(@Path("postId") postId: Int): Single<Post>

    @GET("/users/{userId}")
    fun getUser(@Path("userId") userId: Int): Single<User>

    @GET("/posts/{postId}/comments")
    fun getComments(@Path("postId") postId: Int): Single<List<Comment>>
}