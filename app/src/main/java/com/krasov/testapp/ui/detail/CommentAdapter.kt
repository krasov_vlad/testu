package com.krasov.testapp.ui.detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.krasov.testapp.domain.model.Comment

class CommentAdapter() : RecyclerView.Adapter<CommentAdapter.CommentViewHolder>() {

    private var comments = listOf<Comment>()

    fun setComments(list: List<Comment>) {
        //todo use diffutils here
        comments = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        return CommentViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(android.R.layout.simple_list_item_2, parent, false)
        )
    }

    override fun getItemCount(): Int = comments.size

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        holder.bind(comments[position])
    }

    class CommentViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(comment: Comment) {
            view.findViewById<TextView>(android.R.id.text1)?.text = comment.name
            view.findViewById<TextView>(android.R.id.text2)?.text = comment.body
        }
    }
}