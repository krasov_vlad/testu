package com.krasov.testapp.data.repository

import com.krasov.testapp.data.source.local.ILocalStorage
import com.krasov.testapp.data.source.remote.ApiService
import com.krasov.testapp.domain.model.Comment
import com.krasov.testapp.domain.model.Post
import com.krasov.testapp.domain.model.User
import com.krasov.testapp.domain.repository.IRepository
import io.reactivex.Single


//TODO here we should check if cached value exist and return it if not forceloadign
class MainRepository(
    private val remoteDataSource: ApiService,
    private val localDataSource: ILocalStorage
) : IRepository {

    override fun getPost(postId: Int, forceNetwork: Boolean): Single<Post> {
        return if (forceNetwork) {
            remoteDataSource.getPost(postId)
                .doOnSuccess { localDataSource.savePosts(it) }
        } else {
            Single.just(localDataSource.getPost((postId)))
        }
    }

    override fun getUser(userId: Int, forceNetwork: Boolean): Single<User> {
        return if (forceNetwork) {
            remoteDataSource.getUser(userId)
                .doOnSuccess { localDataSource.saveUser(it) }
        } else {
            Single.just(localDataSource.getUser(userId))
        }
    }

    override fun getPosts(forceNetwork: Boolean): Single<List<Post>> {
        return if (forceNetwork) {
            remoteDataSource.getPosts()
                .doOnSuccess { localDataSource.savePosts(*it.toTypedArray()) }
        } else {
            Single.just(localDataSource.getPosts())
        }
    }

    override fun getComments(postId: Int, forceNetwork: Boolean): Single<List<Comment>> {
        return if (forceNetwork) {
            remoteDataSource.getComments(postId)
                .doOnSuccess { localDataSource.saveComments(it) }
        } else {
            Single.just(localDataSource.getComments(postId))
        }
    }
}