package com.krasov.testapp.di

import com.krasov.testapp.ui.detail.DetailFragment
import com.krasov.testapp.ui.detail.DetailViewModel
import com.krasov.testapp.ui.list.ListFragment
import com.krasov.testapp.ui.list.ListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

private val listModule = module {
    scope(named<ListFragment>()) {
        viewModel { ListViewModel(get()) }
    }
}

private val detailModule = module {
    scope(named<DetailFragment>()) {
        viewModel { (postId: Int) -> DetailViewModel(get(), postId) }
    }
}

val moduleList = listOf(dataModule, domainModule, listModule, detailModule)