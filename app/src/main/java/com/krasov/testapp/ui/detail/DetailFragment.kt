package com.krasov.testapp.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.krasov.testapp.R
import kotlinx.android.synthetic.main.detail_fragment.*
import kotlinx.android.synthetic.main.list_fragment.refresh
import org.koin.androidx.scope.currentScope
import org.koin.core.parameter.parametersOf

class DetailFragment : Fragment() {

    private val viewModel: DetailViewModel by currentScope.inject {
        parametersOf(args.postId)
    }

    private val args: DetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.detail_fragment, container, false)
    }

    private val commentsAdapter by lazy {
        CommentAdapter()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.loadData(false)
        viewModel.toast.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let {
                Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
            }
        })

        viewModel.user.observe(viewLifecycleOwner, Observer {
            //todo extract user fields to separate view for reusability
            user.text = it.name
            userEmail.text = it.email
        })
        viewModel.comments.observe(viewLifecycleOwner, Observer {
            commentsAdapter.setComments(it)
        })
        viewModel.showLoading.observe(viewLifecycleOwner, Observer {
            loadingProgress.isVisible = it
        })
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        commentList.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = commentsAdapter
        }

        refresh.setOnClickListener {
            viewModel.loadData(true)
        }
    }
}