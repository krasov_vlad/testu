package com.krasov.testapp.ui.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.krasov.testapp.domain.MainInteractor
import com.krasov.testapp.domain.model.Post
import com.krasov.testapp.ui.BaseViewModel
import com.krasov.testapp.utils.applyBackgroundTaskSchedulers
import com.krasov.testapp.utils.livedata.RxSingleLiveData
import com.krasov.testapp.utils.livedata.SingleEvent
import io.reactivex.disposables.CompositeDisposable

class ListViewModel(private val interactor: MainInteractor) : BaseViewModel() {

    private val _toast = MutableLiveData<SingleEvent<String>>()
    val toast: LiveData<SingleEvent<String>> = _toast

    private val _posts = MutableLiveData<List<Post>>()
    val posts: LiveData<List<Post>> = _posts

    fun loadPosts(forceLoad: Boolean) {
        subscribe(
            interactor.getPosts(forceLoad || posts.value.isNullOrEmpty())
                .applyBackgroundTaskSchedulers()
                .subscribe({
                    _posts.value = it
                }, {
                    //todo handle error properly
                    _toast.value = SingleEvent(it.message ?: "unknown error")
                })
        )
    }

}