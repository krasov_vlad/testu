package com.krasov.testapp

import android.app.Application
import com.krasov.testapp.di.moduleList
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.core.logger.Logger
import org.koin.core.logger.MESSAGE
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        setupKoinDI()
    }


    private fun setupKoinDI() {
        startKoin {
            androidContext(this@App)

            logger(object : Logger(Level.DEBUG) {
                override fun log(level: Level, msg: MESSAGE) {
                    when (level) {
                        Level.DEBUG -> Timber.tag("Koin").d(msg)
                        Level.INFO -> Timber.tag("Koin").i(msg)
                        Level.ERROR -> Timber.tag("Koin").e(msg)
                    }
                }
            })

            modules(moduleList)
        }
    }
}