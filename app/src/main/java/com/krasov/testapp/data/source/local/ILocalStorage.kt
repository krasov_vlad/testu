package com.krasov.testapp.data.source.local

import com.krasov.testapp.domain.model.Comment
import com.krasov.testapp.domain.model.Post
import com.krasov.testapp.domain.model.User

interface ILocalStorage {
    fun getPosts(): List<Post>
    fun getPost(postId: Int): Post?
    fun getUser(userId: Int): User?
    fun getComments(postId: Int): List<Comment>
    fun savePosts(vararg post: Post)
    fun saveUser(user: User)
    fun saveComments(list: List<Comment>)
    fun clear()
}