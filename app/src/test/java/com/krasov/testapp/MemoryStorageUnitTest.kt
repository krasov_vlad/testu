package com.krasov.testapp

import com.krasov.testapp.data.source.local.MemoryStorage
import com.krasov.testapp.domain.model.Post
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class MemoryStorageUnitTest {

    lateinit var storage: MemoryStorage

    @Before
    fun setUp() {
        storage = MemoryStorage()
    }

    @Test
    fun writePost() {
        storage.savePosts(Post(1, 1, "test", ""), Post(1, 2, "", ""))
        Assert.assertEquals(storage.getPosts().size, 2)
    }

    @Test
    fun readPost() {
        storage.savePosts(Post(1, 1, "test 1", ""), Post(1, 2, "test 2", ""))

        val post = storage.getPost(2)
        Assert.assertNotNull(post)
        Assert.assertEquals(post?.title, "test 2")
    }

    @Test
    fun deleteAllPost() {
        storage.savePosts(Post(1, 1, "test", ""), Post(1, 2, "", ""))
        Assert.assertEquals(storage.getPosts().size, 2)
        storage.clear()
        Assert.assertEquals(storage.getPosts().size, 0)
    }
}