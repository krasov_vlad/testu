package com.krasov.testapp.di

import com.krasov.testapp.BuildConfig
import com.krasov.testapp.data.source.local.ILocalStorage
import com.krasov.testapp.data.source.local.MemoryStorage
import com.krasov.testapp.data.source.remote.ApiService
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val dataModule = module {
    single<ILocalStorage> { MemoryStorage() }

    single { createApiService(get()) }

    single { createRetrofit(get(), BuildConfig.API_BASE_URL) }

    single { createOkHttpClient() }
}


private fun createOkHttpClient(): OkHttpClient {
    return OkHttpClient.Builder()
        .connectTimeout(60L, TimeUnit.SECONDS)
        .readTimeout(60L, TimeUnit.SECONDS)
        .build()
}

private fun createRetrofit(okHttpClient: OkHttpClient, url: String): Retrofit {
    return Retrofit.Builder()
        .baseUrl(url)
        .client(okHttpClient)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

private fun createApiService(retrofit: Retrofit): ApiService {
    return retrofit.create(ApiService::class.java)
}